import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeveloperRoutingModule } from './developer-routing.module';
import { DeveloperComponent } from './developer.component';

@NgModule({
  imports: [
    CommonModule,
    DeveloperRoutingModule
  ],
  declarations: [DeveloperComponent]
})
export class DeveloperModule { }
