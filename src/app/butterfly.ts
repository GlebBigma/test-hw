export class Hero {
    id: number;
    name: string;
    subtitle: string;
    image: string;
    description: string;
    range: string;
    evidence: string;
}
