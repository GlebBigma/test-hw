import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButterfliesRoutingModule } from './butterflies-routing.module';
import { ButterfliesComponent } from './butterflies.component';
import {
  MdButtonModule,
  MdCardModule
} from '@angular/material';
import {HeroDetailComponent} from '../hero-detail.component';
import {DashboardComponent} from '../dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    ButterfliesRoutingModule,
    MdButtonModule,
    MdCardModule
  ],
  declarations: [
    ButterfliesComponent,
    HeroDetailComponent,
    DashboardComponent
  ]
})
export class ButterfliesModule { }
