import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ButterfliesComponent} from './butterflies.component';

const routes: Routes = [
  {path: 'butterflies', component: ButterfliesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ButterfliesRoutingModule { }
