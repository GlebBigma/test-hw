import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { HeroService } from './hero.service'; // test

import {
MdMenuModule,
MdIconModule,
MdButtonModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {DeveloperModule} from './developer/developer.module';
import {ButterfliesModule} from './butterflies/butterflies.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    DeveloperModule,
    ButterfliesModule,
    MdMenuModule,
    MdIconModule,
    MdButtonModule
  ],
  providers: [HeroService],
  bootstrap: [AppComponent]
})
export class AppModule { }
