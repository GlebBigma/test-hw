import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ButterfliesComponent} from './butterflies/butterflies.component';

import { HeroDetailComponent } from './hero-detail.component';

const routes: Routes = [
    {path: '', redirectTo: '/butterflies', pathMatch: 'full'},
    {path: 'butterflies', component: ButterfliesComponent},
    { path: 'detail/:id', component: HeroDetailComponent }
];

@NgModule ({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}
