import { Hero } from './butterfly';

export const HEROES: Hero[] = [
    {
        id: 1,
        name: 'Адмирал',
        subtitle: 'Vanessa atalanta (Linnaeus, 1758)',
        image: 'img-1.jpg',
        description: 'Нимфалиды [Nymphalidae]',
        range: 'Европа - до 62 град. северной широты.',
        evidence: 'Ярко-красная полоса на тёмном фоне и белые пятна в вершинных углах передних крыльев.'
    },
    {
        id: 2,
        name: 'Аргиад',
        subtitle: 'Everes argiades (Pallas, 1771)',
        image: 'img-2.jpg',
        description: 'Голубянки [Lycaenidae]',
        range: 'Европа - Южная и Центральная. Россия. Центральная Азия, Япония.',
        evidence: 'Самка крупнее самца. Окраска самца блестящая голубая с тонкими чёрными точками на кайме задних крыльев.'
    },
    {
        id: 3,
        name: 'Белянка рапсовая',
        subtitle: 'Pontia daplidice (Linnaeus, 1758)',
        image: 'img-3.jpg',
        description: 'Белянки [Pieridae]',
        range: ' Южная и Центральная Европа - вид распространён до 66° северной широты, отдельные экземпляры залетают в Скандинавию.',
        evidence: 'Верхняя сторона крыльев белая с резко выделяющимися жилками и тёмными пятнами в верхней части передних крыльев.'
    },
];
