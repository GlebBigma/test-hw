import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title = 'Butterflies of the world';
  menuItems = [
    {
      title: 'Бабочки',
      route: 'butterflies'
    },
    {
      title: 'Информация',
      route: 'developer-info'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
